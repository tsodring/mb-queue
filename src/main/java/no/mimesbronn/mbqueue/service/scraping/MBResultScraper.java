package no.mimesbronn.mbqueue.service.scraping;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.mimesbronn.mbqueue.model.Request;
import no.mimesbronn.mbqueue.model.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.time.OffsetDateTime.parse;

@Service
public class MBResultScraper {

    @Value("${mb-address:https://www.mimesbronn.no}")
    private String address;
    private Elements results;
    private int size;
    private int currentResult;

    public void connectToService()
        throws IOException {
        Document doc = Jsoup.connect("https://www.mimesbronn" +
            ".no/list/successful?request_date_after=04.10.2019").get();
        //Document doc = Jsoup.connect("https://www.mimesbronn" +
        //      ".no/search/Oslo%20lufthavn,%20sak%20(2019/3540)").get();
        results = doc.select("div.request_left");
        size = results.size();
        currentResult = 0;
    }

    public Request getNextResult() throws IOException, URISyntaxException {
        if (currentResult < size) {
            Element listing = results.get(currentResult++);
            String url = retrieveUrl(listing);
            if (!url.isEmpty()) {
                Request request = parseRequest(url);
                addResponses(url, request);
                return request;
            }
        }
        return null;
    }

    public void addResponses(String url, Request request)
        throws IOException {

        Document doc = Jsoup.connect(url).get();

        var responses = (ArrayList<Response>) request.getReferenceResponses();
        for (Response response : responses) {
            String incomingMessageId = response.getMessageId();
            //Element mbResponse =
            //   doc.select("div[id$=.*-" + incomingMessageId + "]").first();
            Element mbResponse =
                doc.select("div[id=outgoing-" +
                    incomingMessageId + "], " +
                    "div[id=incoming-" + incomingMessageId + "]").first();
            Element a = mbResponse.select("a[href]").first();
            if (a != null) {
                response.setUrl(address + a.attr("href"));
            }
            Element correspondencePart = doc.
                select("div.correspondence__header__from").first();
            response.setCorrespondenceParty(correspondencePart.html().strip());
            System.out.println(response);
        }
    }

    public Request parseRequest(String url) throws IOException, URISyntaxException {
        Request request = new Request();

        // Set the URL without the .json bit required in the next step
        request.setUrl(url);
        url += ".json";

        URL actualUrl = new URL(url);
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.readValue(actualUrl, Map.class);

        request.setTitle((String) map.get("title"));
        request.setCreatedDate(parse((CharSequence) map.get("created_at")));
        request.setUpdatedDate(parse((CharSequence) map.get("updated_at")));
        var user = (Map<String, String>) map.get("user");

        for (var event : (List<Map<String, Object>>)
            map.get("info_request_events")) {

            // We are only looking for messages
            if (event.get("event_type").equals("status_update")) {
                continue;
            }

            Response response = new Response();
            OffsetDateTime eventTime = parse(((String) event.get("created_at")));
            response.setMimesBronnCreatedDate(eventTime);
            response.setMimesBronnId(((Integer) event.get("id")).toString());
            response.setMessageType(((String) event.get("event_type")));
            if (event.get("incoming_message_id") != null) {
                response.setMessageId(
                    ((Integer) event.get("incoming_message_id")).toString());
            }
            if (event.get("outgoing_message_id") != null) {
                response.setMessageId(
                    ((Integer) event.get("outgoing_message_id")).toString());
            }
            request.addReferenceResponses(response);
        }
        System.out.println(request);
        return request;
    }

    private String retrieveUrl(Element element) {
        Element span = element.select("span.head").first();
        if (span != null) {
            Element a = span.select("a[href]").first();
            if (a != null) {
                String url = a.attr("href");
                // Remove the everything before the # symbol
                final Pattern p = Pattern.compile("^(.*)#");
                Matcher m = p.matcher(url);
                if (m.find()) {
                    url = m.group(1);
                }
                return address + url;
            }
        }
        return new String();
    }
}

