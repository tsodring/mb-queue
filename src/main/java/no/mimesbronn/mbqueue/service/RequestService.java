package no.mimesbronn.mbqueue.service;

import no.mimesbronn.mbqueue.error.NotFoundException;
import no.mimesbronn.mbqueue.model.Request;
import no.mimesbronn.mbqueue.repository.IRequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
@Transactional
public class RequestService
    implements IRequestService {

    private static final Logger logger =
        LoggerFactory.getLogger(RequestService.class);

    private IRequestRepository requestRepository;

    public RequestService(IRequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    @Override
    public Page<Request> getAllRequestsPaginated(Integer page, Integer size) {
        return requestRepository.findAll(PageRequest.of(page, size));
    }

    @Override
    public Page<Request> getAllRequestsContainingPaginated(
        String query, Integer page, Integer size) {
        return requestRepository.findByTitleContaining(query,
            PageRequest.of(page, size));
    }

    @Override
    public Request getRequest(@NotNull Long requestId) {
        return getRequestOrThrow(requestId);
    }

    @Override
    public Optional<Request> getByUrl(String url) {
        return requestRepository.findByUrl(url);
    }

    @Override
    public Request saveRequest(@NotNull Request request) {
        return requestRepository.save(request);
    }

    @Override
    public void deleteRequest(@NotNull Long requestId) {
        requestRepository.delete(getRequestOrThrow(requestId));
    }

    @Override
    public void verifyRequest(@NotNull Long requestId) {
        Request existingRequest = getRequestOrThrow(requestId);
        existingRequest.setChecked(true);
        requestRepository.save(existingRequest);
    }

    @Override
    public Request updateRequest(@NotNull Long requestId,
                              @NotNull Request request) {
        Request existingRequest = getRequestOrThrow(requestId);
        existingRequest.setTitle(request.getTitle());
        existingRequest.setUrl(request.getUrl());
        return requestRepository.save(existingRequest);
    }

    /**
     * find the request or throw a 404
     *
     * @param requestId Id of the request to find
     * @return the request object
     */
    protected Request getRequestOrThrow(@NotNull Long requestId) {
        Optional<Request> requestOptional =
            requestRepository.findById(requestId);

        if (requestOptional.isPresent())
            return requestOptional.get();
        else {
            String info = "Request with id (" + requestId + ") NOT FOUND";
            logger.error(info);
            throw new NotFoundException(info);
        }
    }
}
