package no.mimesbronn.mbqueue.service;

import no.mimesbronn.mbqueue.model.MBUser;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;

public interface IUserService {

    Page<MBUser> getAllUsersPaginated(Integer page, Integer size);

    Page<MBUser> getAllUsersContainingPaginated(
        String query, Integer page, Integer size);

    MBUser getUser(@NotNull Long UserId);

    void deleteUser(@NotNull Long UserId);

    MBUser updateUser(@NotNull Long UserId, @NotNull MBUser MBUser);
}
