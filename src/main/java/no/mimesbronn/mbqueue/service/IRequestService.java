package no.mimesbronn.mbqueue.service;

import no.mimesbronn.mbqueue.model.Request;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface IRequestService {

    Page<Request> getAllRequestsPaginated(Integer page, Integer size);

    Page<Request> getAllRequestsContainingPaginated(
        String query, Integer page, Integer size);

    Optional<Request> getByUrl(String url);

    Request getRequest(@NotNull Long requestId);

    Request saveRequest(@NotNull Request request);

    void deleteRequest(@NotNull Long requestId);

    Request updateRequest(@NotNull Long requestId, @NotNull Request request);

    void verifyRequest(@NotNull Long requestId);
}
