package no.mimesbronn.mbqueue.service;

import no.mimesbronn.mbqueue.error.NotFoundException;
import no.mimesbronn.mbqueue.model.MBUser;
import no.mimesbronn.mbqueue.repository.IMBUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
@Transactional
public class UserService
    implements IUserService {

    private static final Logger logger =
        LoggerFactory.getLogger(UserService.class);

    private IMBUserRepository userRepository;

    public UserService(IMBUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Page<MBUser> getAllUsersPaginated(Integer page, Integer size) {
        return userRepository.findAll(PageRequest.of(page, size));
    }

    @Override
    public Page<MBUser> getAllUsersContainingPaginated(
        String query, Integer page, Integer size) {
        return userRepository.findByUsernameContaining(query,
            PageRequest.of(page, size));
    }

    @Override
    public MBUser getUser(@NotNull Long UserId) {
        return getUserOrThrow(UserId);
    }

    @Override
    public void deleteUser(@NotNull Long UserId) {
        userRepository.delete(getUserOrThrow(UserId));
    }

    @Override
    public MBUser updateUser(@NotNull Long UserId,
                             @NotNull MBUser MBUser) {
        MBUser existingMBUser = getUserOrThrow(UserId);
        existingMBUser.setFirstname(MBUser.getFirstname());
        existingMBUser.setLastname(MBUser.getLastname());
        return userRepository.save(existingMBUser);
    }

    /**
     * find the User or throw a 404
     *
     * @param UserId Id of the User to find
     * @return the User object
     */
    protected MBUser getUserOrThrow(@NotNull Long UserId) {
        Optional<MBUser> UserOptional =
            userRepository.findById(UserId);

        if (UserOptional.isPresent())
            return UserOptional.get();
        else {
            String info = "User with id (" + UserId + ") NOT FOUND";
            logger.error(info);
            throw new NotFoundException(info);
        }
    }
}
