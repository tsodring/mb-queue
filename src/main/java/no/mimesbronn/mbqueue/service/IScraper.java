package no.mimesbronn.mbqueue.service;

import java.io.IOException;

public interface IScraper {
    void scrape(Integer fromId, Integer toId) throws IOException;
}
