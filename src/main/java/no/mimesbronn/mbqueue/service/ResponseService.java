package no.mimesbronn.mbqueue.service;

import no.mimesbronn.mbqueue.error.NotFoundException;
import no.mimesbronn.mbqueue.model.Response;
import no.mimesbronn.mbqueue.repository.IResponseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
@Transactional
public class ResponseService
    implements IResponseService {

    private static final Logger logger =
        LoggerFactory.getLogger(ResponseService.class);

    private IResponseRepository responseRepository;

    public ResponseService(IResponseRepository responseRepository) {
        this.responseRepository = responseRepository;
    }

    @Override
    public Page<Response> getAllResponsesPaginated(Integer page, Integer size) {
        return responseRepository.findAll(PageRequest.of(page, size));
    }

    @Override
    public Response getResponse(@NotNull Long ResponseId) {
        return getResponseOrThrow(ResponseId);
    }

    @Override
    public void deleteResponse(@NotNull Long responseId) {
        responseRepository.delete(getResponseOrThrow(responseId));
    }

    @Override
    public Response saveResponse(@NotNull Response response) {
        return responseRepository.save(response);
    }

    @Override
    // Should have a HasRole("REQUEST_MANAGER")
    public Response updateResponse(@NotNull Long responseId,
                                   @NotNull Response response) {
        Response existingResponse = getResponseOrThrow(responseId);
        existingResponse.setUrl(response.getUrl());
        return existingResponse;
    }

    @Override
    public void verifyResponse(@NotNull Long responseId) {
        Response existingResponse = getResponseOrThrow(responseId);
        existingResponse.setChecked(true);
        responseRepository.save(existingResponse);
    }

    /**
     * find the Response or throw a 404
     *
     * @param ResponseId Id of the Response to find
     * @return the Response object
     */
    protected Response getResponseOrThrow(@NotNull Long ResponseId) {
        Optional<Response> ResponseOptional =
            responseRepository.findById(ResponseId);

        if (ResponseOptional.isPresent())
            return ResponseOptional.get();
        else {
            String info = "Response with id (" + ResponseId + ") NOT FOUND";
            logger.error(info);
            throw new NotFoundException(info);
        }
    }
}
