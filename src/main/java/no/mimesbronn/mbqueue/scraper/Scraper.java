package no.mimesbronn.mbqueue.scraper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.h2.Driver;
import org.h2.jdbcx.JdbcDataSource;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Scraper {
    private final CloseableHttpClient httpclient;
    private final Gson gson;


    public static void main(String[] args) throws IOException, SQLException {
        // :: Connect to database
        DriverManager.registerDriver(new Driver());
        JdbcDataSource jdbcDataSource = new JdbcDataSource();
        jdbcDataSource.setUrl("jdbc:h2:file:./mimesbronn.h2");
        jdbcDataSource.setUser("sa");
        jdbcDataSource.setPassword("");

        ScraperDb scraperDb = new ScraperDb(jdbcDataSource);
        scraperDb.createTablesIfNotExists();

        // :: Get some events
        Scraper scraper = new Scraper();
        scraper.scrapeNewMessagesFromMimesBronn(scraperDb);
    }

    public void scrapeNewMessagesFromMimesBronn(ScraperDb scraperDb) throws IOException {
        long eventId = 21408;
        Optional<Long> maxRequestEventId = scraperDb.getMaxRequestEventId();
        if (maxRequestEventId.isPresent()) {
            // -> We have requestEvents in database.
            eventId = maxRequestEventId.get().longValue();
        }

        int requestEvents_on_thisRun = 0;
        while (true) {
            eventId++;
            requestEvents_on_thisRun++;

            if (requestEvents_on_thisRun >= 250) {
                System.err.println("We have downloaded " + requestEvents_on_thisRun + " request events." +
                        " Halting scraper here to give the server a break.");
                break;
            }

            if (scraperDb.requestEventExists(eventId)) {
                // -> Request event already exists in database. No need to get it for Mimes Brønn again.
                continue;
            }
            Optional<RequestEvent> event = getEvent(eventId);
            if (event.isEmpty()) {
                System.err.println("[" + eventId + "] not present. Halting scraper here.");
                break;
            }

            RequestEvent requestEvent = event.get();
            if (requestEvent.messageType.isEmpty()) {
                scraperDb.insertRequstEvent_notMessage(eventId, "Found. Not message.");
                System.err.println("[" + eventId + "] not incoming/outgoing");
                continue;
            }

            System.err.println("[" + eventId + "] is a message, getting full request");
            if (!scraperDb.requestExists(requestEvent.requestUrlTitle)) {
                // -> We don't have this request, let's get it.
                AlaveteliRequestDto requestDto = getRequest(requestEvent.requestUrl);
                scraperDb.insertRequest(
                        requestDto.id,
                        requestDto.url_title,
                        requestDto.title,
                        requestDto.created_at,
                        requestDto.updated_at,
                        requestDto.user.url_name,
                        requestDto.public_body.url_name,
                        // Save a simple string of the dags
                        requestDto.tags.entrySet().stream()
                                .map(e -> e.getKey() + ":" + e.getValue())
                                .collect(Collectors.joining(" ")),
                        requestDto.json
                );
                System.err.println(requestDto.title);
            }
            else {
                // TODO: We should update if we don't already have the event in the request
            }

            scraperDb.insertRequestEvent(eventId,
                    "Found. Message.",
                    requestEvent.requestUrl,
                    requestEvent.requestUrlTitle,
                    requestEvent.messageType.get(),
                    requestEvent.messageId.get());
        }
    }

    public Scraper() {
        httpclient = HttpClientBuilder.create()
                .disableRedirectHandling()
                .build();
        gson = new GsonBuilder()
                .create();
    }


    /**
     * Get the URL for a given event. An event can be incoming or outgoing message in a request.
     *
     * @param eventId Event ID. As of 06.11.2019 Mimes Brønn have over 20000 events.
     * @return The event contain URL to parent, ID, etc
     */
    public Optional<RequestEvent> getEvent(long eventId) throws IOException {
        String url = "https://www.mimesbronn.no/request_event/" + eventId;
        HttpResponse response = httpclient.execute(auth(new HttpGet(url)));
        ((CloseableHttpResponse) response).close();

        if (response.getStatusLine().getStatusCode() == 404) {
            return Optional.empty();
        } else if (response.getStatusLine().getStatusCode() == 301) {
            return Optional.of(new RequestEvent(eventId, response.getFirstHeader("Location").getValue()));
        }

        throw new RuntimeException("Unknown response on [" + url + "]:\n"
                + Stream.of(response.getAllHeaders())
                .map(Object::toString)
                .collect(Collectors.joining("\n")));
    }

    /**
     * @param requestUrl URL for the request. E.g. https://www.mimesbronn.no/request/tilgangskoder_og_enheter_429#outgoing-5500
     */
    private AlaveteliRequestDto getRequest(String requestUrl) throws IOException {
        HttpResponse response = httpclient.execute(auth(new HttpGet(requestUrl + ".json")));

        String body = IOUtils.toString(response.getEntity().getContent(), "utf8");
        AlaveteliRequestDto requestDto = gson.fromJson(body, AlaveteliRequestDto.class);
        ((CloseableHttpResponse) response).close();
        requestDto.json = body;

        return requestDto;
    }

    private HttpRequestBase auth(HttpRequestBase httpRequest) {
        httpRequest.setHeader("Cookie", "mimesbronn=full_access");
        return httpRequest;
    }

    private static class AlaveteliRequestDto {
        long id;
        String url_title;
        String title;
        String created_at;
        String updated_at;
        Map<String, Object> tags;
        AlaveteliUserDto user;
        AlaveteliPublicBodyDto public_body;
        AlaveteliRequestEventDto[] info_request_events;

        /**
         * Full JSON response from https://www.mimesbronn.no/request/<url_title>.json. The other fields are read from
         * this.
         */
        String json;
    }

    private static class AlaveteliUserDto {
        long id;
        String name;
        String url_name;
    }

    private static class AlaveteliRequestEventDto {
        long id;
        String event_type;
        long incoming_message_id;
        long outgoing_message_id;
    }

    private static class AlaveteliPublicBodyDto {
        long id;
        String name;
        String url_name;
    }

    public static class RequestEvent {
        private final long eventId;
        private final String requestUrl;
        private final String requestUrlTitle;
        private final Optional<String> messageType;
        private final Optional<String> messageId;

        public RequestEvent(long eventId, String urlWithHash) {
            this.eventId = eventId;

            // :: Regex examples
            // event id 20000 - event type sent, points to outgoing message id 5500
            //     https://www.mimesbronn.no/request/tilgangskoder_og_enheter_429#outgoing-5500
            // event id 21303 - event type status_update
            //     https://www.mimesbronn.no/request/orientering_om_skraping_av_einns
            Pattern pattern = Pattern.compile("^(https:\\/\\/www\\.mimesbronn\\.no\\/request\\/(([A-Za-z_0-9]*)))(#([A-Za-z_0-9]*)\\-([A-Za-z_0-9]*))?$");
            Matcher matcher = pattern.matcher(urlWithHash);
            if (!matcher.find()) {
                throw new RuntimeException("The URL [" + urlWithHash + "] did not match regex. Must likely adjust regex.");
            }
            this.requestUrl = matcher.group(1);
            this.requestUrlTitle = matcher.group(2);
            this.messageType = Optional.ofNullable(matcher.group(5));
            this.messageId = Optional.ofNullable(matcher.group(6));

            // Data quality, check that we know all types of messages so that our assumtion about event ids just being outgoing and incmming messages are true
            if (
                    this.messageType.isPresent()
                            && !this.messageType.get().equals("outgoing")
                            && !this.messageType.get().equals("incoming")
            ) {
                throw new RuntimeException("The URL [" + urlWithHash + "] did not return a known message type: " + this.messageType);
            }
        }
    }
}
