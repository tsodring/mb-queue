package no.mimesbronn.mbqueue.scraper;

import no.mimesbronn.mbqueue.model.Request;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ScraperDb {

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    public ScraperDb(DataSource dataSource) {
        this(new JdbcTemplate(dataSource));
    }

    public ScraperDb(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public void createTablesIfNotExists() {
        try {
            jdbcTemplate.getDataSource().getConnection()
                    .prepareStatement("CREATE TABLE IF NOT EXISTS mb_request_events(" +
                            "eventId INT PRIMARY KEY," +
                            "status VARCHAR(20), " +
                            "requestUrl VARCHAR(255), " +
                            "requestUrlTitle VARCHAR(255), " +
                            "messageType VARCHAR(255), " +
                            "messageId VARCHAR(255)" +
                            ");").executeUpdate();
            jdbcTemplate.getDataSource().getConnection()
                    .prepareStatement("CREATE TABLE IF NOT EXISTS mb_requests(" +
                            "id INT PRIMARY KEY," +
                            "urlTitle VARCHAR(255) NOT NULL, " +
                            "title VARCHAR(255) NOT NULL, " +
                            "createdAt VARCHAR(255) NOT NULL, " +
                            "updatedAt VARCHAR(255) NOT NULL," +
                            "userUrlName VARCHAR(255) NOT NULL, " +
                            "publicBodyUrlName VARCHAR(255) NOT NULL, " +
                            "tags VARCHAR(255) NULL, " +
                            "json TEXT NOT NULL" +
                            ");").executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Unable to create tables.", e);
        }
    }

    public boolean requestEventExists(long eventId) {
        return jdbcTemplate.queryForObject("SELECT count(*) FROM mb_request_events WHERE eventId = ?",
                new Object[]{Long.valueOf(eventId)},
                Integer.class) > 0;
    }

    public void insertRequstEvent_notMessage(long eventId, String status) {
        Map<String, Object> params = new HashMap<>();
        params.put("eventId", eventId);
        params.put("status", status);
        namedJdbcTemplate.update("INSERT INTO mb_request_events (eventId, status) " +
                "VALUES (:eventId, :status)", params);
    }

    public void insertRequestEvent(long eventId, String status, String requestUrl, String requestUrlTitle,
                                   String messageType,
                                   String messageId) {
        Map<String, Object> params = new HashMap<>();
        params.put("eventId", eventId);
        params.put("status", status);
        params.put("requestUrl", requestUrl);
        params.put("requestUrlTitle", requestUrlTitle);
        params.put("messageType", messageType);
        params.put("messageId", messageId);
        namedJdbcTemplate.update("INSERT INTO mb_request_events" +
                " (eventId, status, requestUrl, requestUrlTitle, messageType, messageId) " +
                "VALUES (:eventId, :status, :requestUrl, :requestUrlTitle, :messageType, :messageId)", params);
    }

    public void insertRequest(long id, String urlTitle, String title, String createdAt, String updatedAt,
                              String userUrlName, String publicBodyUrlName, String tags, String json) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("urlTitle", urlTitle);
        params.put("title", title);
        params.put("createdAt", createdAt);
        params.put("updatedAt", updatedAt);
        params.put("userUrlName", userUrlName);
        params.put("publicBodyUrlName", publicBodyUrlName);
        params.put("tags", tags);
        params.put("json", json);
        namedJdbcTemplate.update("INSERT INTO mb_requests" +
                        " (id, urlTitle, title, createdAt, updatedAt, userUrlName, publicBodyUrlName, tags, json) " +
                        "VALUES (:id, :urlTitle, :title, :createdAt, :updatedAt, :userUrlName, :publicBodyUrlName, :tags, :json)",
                params);
    }

    public boolean requestExists(String requestUrlTitle) {
        return jdbcTemplate.queryForObject("SELECT count(*) FROM mb_requests WHERE urlTitle = ?",
                new Object[]{requestUrlTitle},
                Integer.class) > 0;
    }

    public Optional<Long> getMaxRequestEventId() {
        return Optional.ofNullable(jdbcTemplate.queryForObject("SELECT max(eventId) FROM mb_request_events",
                Long.class));
    }

    public List<Request> getRequests() {
        return jdbcTemplate.queryForList("SELECT * FROM mb_requests")
                .stream().map(row -> {
                    Request request = new Request();
                    request.setTitle((String) row.get("title"));
                    request.setUrl("https://www.mimesbronn.no/request/" + row.get("urltitle"));
                    return request;
                })

                .collect(Collectors.toList());

    }
}
