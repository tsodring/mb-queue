package no.mimesbronn.mbqueue.repository;

import no.mimesbronn.mbqueue.model.Response;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IResponseRepository
    extends PagingAndSortingRepository<Response, Long> {
    Page<Response> findAll(Pageable pageable);
}
