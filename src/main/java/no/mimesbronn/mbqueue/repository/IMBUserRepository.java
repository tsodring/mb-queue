package no.mimesbronn.mbqueue.repository;

import no.mimesbronn.mbqueue.model.MBUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IMBUserRepository
    extends PagingAndSortingRepository<MBUser, Long> {
    Page<MBUser> findAll(Pageable pageable);

    Page<MBUser> findByUsernameContaining(String query, Pageable pageable);
}
