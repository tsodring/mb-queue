package no.mimesbronn.mbqueue.repository;

import no.mimesbronn.mbqueue.model.Comment;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ICommentRepository
    extends PagingAndSortingRepository<Comment, Long> {
}
