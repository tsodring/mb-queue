package no.mimesbronn.mbqueue.controller;

import no.mimesbronn.mbqueue.model.Request;
import no.mimesbronn.mbqueue.scraper.Scraper;
import no.mimesbronn.mbqueue.scraper.ScraperDb;
import no.mimesbronn.mbqueue.service.IRequestService;
import no.mimesbronn.mbqueue.service.IScraper;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

import static no.mimesbronn.mbqueue.config.Constants.SCRAPE;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = SCRAPE)
public class ScraperController {

    private IScraper scraper;
    private final Scraper scraper2;
    private final ScraperDb scraperDb;
    private final IRequestService requestService;

    public ScraperController(IScraper scraper, IRequestService requestService, JdbcTemplate jdbcTemplate) {
        this.scraper = scraper;
        this.requestService = requestService;

        scraper2 = new Scraper();
        scraperDb = new ScraperDb(jdbcTemplate);
        scraperDb.createTablesIfNotExists();
    }

    @PostMapping(value = "/index")
    public ResponseEntity<String> index(
        @RequestParam(defaultValue = "1", required = false) Integer from,
        @RequestParam(required = false) Integer to)
        throws IOException {
        scraper.scrape(from, to);
        return ResponseEntity.status(OK).body("OK");
    }
    @RequestMapping(value = "/scraper2")
    public ResponseEntity<String> scraperHallvard() throws IOException {
        this.scraper2.scrapeNewMessagesFromMimesBronn(scraperDb);
        List<Request> requests = scraperDb.getRequests();
        requests.forEach(request -> {
            requestService.saveRequest(request);
        });

        return ResponseEntity.status(OK).body("OK");
    }


}
