package no.mimesbronn.mbqueue.controller;

import no.mimesbronn.mbqueue.model.MBUser;
import no.mimesbronn.mbqueue.service.IUserService;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static no.mimesbronn.mbqueue.config.Constants.USER;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = USER)
public class UserController {

    private IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<CollectionModel<MBUser>> getAllUsers(
        @RequestParam(required = false) String query,
        @RequestParam(defaultValue = "0", required = false) int page,
        @RequestParam(defaultValue = "10", required = false) int size) {
        var result = new CollectionModel<>(doQuery(query, page, size),
            linkTo(UserController.class).withSelfRel());
        return ResponseEntity.status(OK).body(result);
    }

    @GetMapping(value = "/{userId}")
    public ResponseEntity<MBUser> getUser(
        @PathVariable("userId") Long userId) {
        MBUser MBUser = userService.getUser(userId);
        MBUser.add(linkTo(UserController.class).
            slash(MBUser.getId()).withSelfRel());
        return ResponseEntity.status(OK).body(MBUser);
    }

    @PutMapping(value = "/{userId}")
    public ResponseEntity<MBUser> updateUser(
        @PathVariable("userId") Long userId,
        @RequestBody MBUser updatedMBUser) {
        MBUser MBUser = userService.updateUser(userId,
            updatedMBUser);
        MBUser.add(linkTo(UserController.class).
            slash(MBUser.getId()).withSelfRel());
        return ResponseEntity.status(OK)
            .body(MBUser);
    }

    @DeleteMapping(value = "/{userId}")
    public ResponseEntity<Boolean> deleteUser(
        @PathVariable("userId") Long userId) {
        userService.deleteUser(userId);
        return ResponseEntity.status(OK).body(true);
    }

    private Page<MBUser> doQuery(String query, Integer page, Integer size) {
        if (query != null) {
            return userService.
                getAllUsersContainingPaginated(query, page, size);
        } else {
            return userService.
                getAllUsersPaginated(page, size);
        }
    }
}
