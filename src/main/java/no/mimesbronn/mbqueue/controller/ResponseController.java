package no.mimesbronn.mbqueue.controller;

import no.mimesbronn.mbqueue.model.Response;
import no.mimesbronn.mbqueue.service.IResponseService;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static no.mimesbronn.mbqueue.config.Constants.RESPONSE;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = RESPONSE)
public class ResponseController {

    private IResponseService responseService;

    public ResponseController(IResponseService responseService) {
        this.responseService = responseService;
    }

    @GetMapping
    public ResponseEntity<CollectionModel<Response>> getAllResponses(
        @RequestParam(required = false) String query,
        @RequestParam(defaultValue = "0", required = false) int page,
        @RequestParam(defaultValue = "10", required = false) int size) {
        var result = new CollectionModel<>(doQuery(query, page, size),
            linkTo(ResponseController.class).withSelfRel());
        return ResponseEntity.status(OK).body(result);
    }

    @GetMapping(value = "/{responseId}")
    public ResponseEntity<Response> getResponse(
        @PathVariable("responseId") Long responseId) {
        Response response = responseService.getResponse(responseId);
        response.add(linkTo(ResponseController.class).
            slash(response.getId()).withSelfRel());
        return ResponseEntity.status(OK).body(response);
    }

    @PostMapping(value = "/{responseId}/verify")
    public ResponseEntity<Boolean> verifyResponse(
        @PathVariable("responseId") Long responseId) {
        responseService.verifyResponse(responseId);
        return ResponseEntity.status(OK).body(true);
    }

    @PutMapping(value = "/{responseId}")
    public ResponseEntity<Response> updateResponse(
        @PathVariable("responseId") Long responseId,
        @RequestBody Response updatedResponse) {
        Response response = responseService.updateResponse(responseId,
            updatedResponse);
        response.add(linkTo(ResponseController.class).
            slash(response.getId()).withSelfRel());
        return ResponseEntity.status(OK)
            .body(response);
    }

    @DeleteMapping(value = "/{responseId}")
    public ResponseEntity<Boolean> deleteResponse(
        @PathVariable("responseId") Long responseId) {
        responseService.deleteResponse(responseId);
        return ResponseEntity.status(OK).body(true);
    }

    private Page<Response> doQuery(String query, Integer page, Integer size) {
        return responseService.
            getAllResponsesPaginated(page, size);
    }
}
