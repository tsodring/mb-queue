package no.mimesbronn.mbqueue.controller;

import no.mimesbronn.mbqueue.model.Request;
import no.mimesbronn.mbqueue.service.IRequestService;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static no.mimesbronn.mbqueue.config.Constants.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = REQUEST)
public class RequestController {

    private IRequestService requestService;

    public RequestController(IRequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping
    public ResponseEntity<CollectionModel<Request>> getAllRequests(
        @RequestParam(required = false) String query,
        @RequestParam(defaultValue = "0", required = false) int page,
        @RequestParam(defaultValue = "10", required = false) int size) {
        var result = new CollectionModel<>(doQuery(query, page, size),
            linkTo(RequestController.class).withSelfRel());
        return ResponseEntity.status(OK).body(result);
    }

    @GetMapping(value = "/{requestId}")
    public ResponseEntity<Request> getRequest(
        @PathVariable("requestId") Long requestId) {
        Request request = requestService.getRequest(requestId);
        request.add(linkTo(RequestController.class).
            slash(request.getId()).withSelfRel());
        return ResponseEntity.status(OK).body(request);
    }

    @PutMapping(value = "/{requestId}")
    public ResponseEntity<Request> updateRequest(
        @PathVariable("requestId") Long requestId,
        @RequestBody Request updatedRequest) {
        Request request = requestService.updateRequest(requestId,
            updatedRequest);
        request.add(linkTo(RequestController.class).
            slash(request.getId()).withSelfRel());
        return ResponseEntity.status(OK)
            .body(request);
    }

    @PostMapping(value = "/{requestId}/verify")
    public ResponseEntity<Boolean> verifyRequest(
        @PathVariable("requestId") Long requestId) {
        requestService.verifyRequest(requestId);
        return ResponseEntity.status(OK).body(true);
    }

    @DeleteMapping(value = "/{requestId}")
    public ResponseEntity<Boolean> deleteRequest(
        @PathVariable("requestId") Long requestId) {
        requestService.deleteRequest(requestId);
        return ResponseEntity.status(OK).body(true);
    }

    private Page<Request> doQuery(String query, Integer page, Integer size) {
        if (query != null) {
            return requestService.
                getAllRequestsContainingPaginated(query, page, size);
        } else {
            return requestService.
                getAllRequestsPaginated(page, size);
        }
    }
}
