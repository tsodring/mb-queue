package no.mimesbronn.mbqueue.model;

// import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Entity
public class Request
    extends RepresentationModel {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    /**
     * address of the request
     */
    @NotNull
    @Column(nullable = false, unique = true)
    // @Audited
    private String url;

    /**
     * title of the request
     */
    @NotNull
    @Column(nullable = false)
    // @Audited
    private String title;

    @Column
    @DateTimeFormat(iso = DATE_TIME)
    private OffsetDateTime createdDate;

    @Column
    @DateTimeFormat(iso = DATE_TIME)
    private OffsetDateTime updatedDate;

    @NotNull
    @Column(nullable = false)
    // @Audited
    private boolean checked = false;

    // Link to responses
    @OneToMany(mappedBy = "referenceRequest")
    private List<Response> referenceResponses = new ArrayList<>();

    // Link to comments
    @OneToMany(mappedBy = "referenceRequest")
    private List<Comment> referenceComments = new ArrayList<>();

    // Link to Alaveteli User
    @OneToOne
    private User user;

    // Link to Public body
    @OneToOne
    private PublicBody publicBody;

    public Long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public OffsetDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(OffsetDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Comment> getReferenceComments() {
        return referenceComments;
    }

    public void addReferenceComments(Comment comment) {
        this.referenceComments.add(comment);
    }

    public List<Response> getReferenceResponses() {
        return referenceResponses;
    }

    public void addReferenceResponses(Response response) {
        this.referenceResponses.add(response);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PublicBody getPublicBody() {
        return publicBody;
    }

    public void setPublicBody(PublicBody publicBody) {
        this.publicBody = publicBody;
    }

    @Override
    public String toString() {
        return "Request{" +
            "id=" + id +
            ", url='" + url + '\'' +
            ", title='" + title + '\'' +
            ", createdDate=" + createdDate +
            ", updatedDate=" + updatedDate +
            ", checked=" + checked +
            '}';
    }

}
