package no.mimesbronn.mbqueue.model;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Note: Attribution to @HNygard
 */
public class AlaveteliEvent {
    private final int eventId;
    private final String requestUrl;
    private final String requestUrlTitle;
    private final Optional<String> messageType;
    private final Optional<String> messageId;

    public AlaveteliEvent(int eventId, String urlWithHash) {
        this.eventId = eventId;

        // :: Regex examples
        // event id 20000 - event type sent, points to outgoing message id 5500
        //     https://www.mimesbronn.no/request/tilgangskoder_og_enheter_429#outgoing-5500
        // event id 21303 - event type status_update
        //     https://www.mimesbronn.no/request/orientering_om_skraping_av_einns
        Pattern pattern = Pattern.compile("^(https:\\/\\/www\\.mimesbronn\\.no\\/request\\/(([A-Za-z_0-9]*)))(#([A-Za-z_0-9]*)\\-([A-Za-z_0-9]*))?$");
        Matcher matcher = pattern.matcher(urlWithHash);
        if (!matcher.find()) {
            throw new RuntimeException("The URL [" + urlWithHash + "] did not match regex. Must likely adjust regex.");
        }

        if (eventId == 3505) {
            System.out.println("Here");
        }
        this.requestUrl = matcher.group(1);
        this.requestUrlTitle = matcher.group(2);
        this.messageType = Optional.ofNullable(matcher.group(5));
        this.messageId = Optional.ofNullable(matcher.group(6));

        // Data quality, check that we know all types of messages so that our assumtion about event ids just being outgoing and incmming messages are true
        if (
            this.messageType.isPresent()
                && !this.messageType.get().equals("outgoing")
                && !this.messageType.get().equals("incoming")
        ) {
            throw new RuntimeException("The URL [" + urlWithHash + "] did not return a known message type: " + this.messageType);
        }
    }

    public Response getResponse() {
        Response response = new Response();
        response.setCorrespondenceParty(requestUrlTitle);
        response.setUrl(requestUrl);
        if (messageType.isPresent()) {
            response.setMessageType(messageType.get());
        }
        if (messageId.isPresent()) {
            response.setMessageId(messageId.get());
        }
        return response;
    }
    public int getEventId() {
        return eventId;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public String getRequestUrlTitle() {
        return requestUrlTitle;
    }

    public Optional<String> getMessageType() {
        return messageType;
    }

    public Optional<String> getMessageId() {
        return messageId;
    }
}
